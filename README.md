# Informacje dla serwera Kiedyś to Było DnD
[Zaproszenie](https://discord.gg/5cEYdmxMYC)

# Funkcje
- Tabelka z kosztami magicznych przedmiotów
- Kalkulator nagród za sesje i dla DMów
- Opis instalacji Better20
- Odnośnik do strony ze zwykłymi przedmiotami

# Chcę pomóc
To super, pomocy nigdy dość. Najprostszym sposobem na pomoc jest zrobienie
forka, wstawienie swoich zmian i wystawienie Merge Requesta, ale jeśli nie masz
pojęcia co właśnie powiedziałem to możesz zmieniony kod po prostu wysłać mi na
privie. Instrukcje odpalania lokalnej wersji będą poniżej.

## Jak dokładnie chcesz pomóc?

### Chcę zmienić kolorystykę strony na lepszą
Przyznaję, że nie umiem w design, więc kolory były wybierane na zasadzie "nie
jest tragicznie".

Jeśli znasz CSS/SCSS to folder `src/scss` zawiera wszystkie style. Jeśli nie
masz pojęcia czym jest CSS/SCSS, ale rozumiesz szesnastkowy format kolorów (np.
`#ffffff`), w pliku `src/scss/_colors.scss` znajdziesz definicje kolorów
używanych na stronie.

Wiem że te nazwy kolorów czasem nie mówią za dużo. Niedługo je pozmieniam na
lepsze.

### Twoje zdania są bardziej po polskiemu niż po polsku
True. Nie jestem pisarzem. Tekst znajdziesz w plikach `.html`, zmieniaj do woli
byle by nie ruszać samej struktury strony.

Jeśli nie znasz HTML, po prostu zmieniaj sam tekst i nie dotykaj znaczników (np.
`<div>`, `<a>`). Jeśli znasz to postaraj się nie zmieniać samej struktury za
bardzo.

### Umiem HTML i CSS i chcę pomóc z designem.
To super. Masz tutaj wszystko, miłej zabawy. I tak, wiem że mój kod to w
najlepszym przypadku 7/10, ale działa. Zazwyczaj. Niedługo planuję zrobić pełny
refactor.

### Chcę dodać kolejne narzędzie. Znam HTML, CSS i JS
Każde narzędzie ma osobny plik `.html` i oprócz tego własny skrypt w JS. Style
są globalne, ale to się pewnie niedługo zmieni więc nie traktuj tego jako zasady
i na spokojnie rób style w osobnym arkuszu.

### Nie umiem kodować, ale i tak chcę pomóc
Jedną z rzeczy które chcę zaimplementować jest dodanie opisów do magicznych
przedmiotów. Możesz pomóc poprzez dopisywanie ich do itemków. Opis pliku z
itemami jest niżej.

Jeśli chcesz dopisać opis po prostu na końcu linijki wstaw
kolejny przecinek, a po nim opis w cudzysłowie. Jeśli gdzieś w opisie będzie
cudzysłów zamień go na pojedynczy, czyli `"` zmienia się w `'`.

Opis się nie wyświetli w tabelce, bo jeszcze nie ma na niego kodu.
## Plik z magicznymi itemami
Lista przedmiotów jest w pliku `magic_items.csv`. Jest to tzw. plik CSV (comma
separated values), czyli każda linijka to jeden item (pomijając pierwszą), a
poszczególne informacje są rozdzielone przecinkami.

Format tego pliku to:
```
[Nazwa przedmiotu],[Czy wymaga attunementu (y/n)],[Koszt w RES],[Kosz w
GP],[Koszt w DT],[Crafting mod],[RES za rozłożenie],[Wymagane
komponenty],[Wymagane narzędzia]
```

Jeżeli gdzieś musi być przecinek, np. gdy wymagane jest kilka komponentów, weź
całość w cudzysłów.

Przykład definicji jednego itema:
```
Bead of Nourishment,n,2,1,1,3,1,,"Alchemist's supplies (Int or Wis), Cook's utensils (Con or Wis)"
```
Czyli Bead of Nourishment nie wymaga attunementu, kosztuje 2RES, 1GP i 1DT,
jego crafting mod jest równy 3, za rozłożenie go dostanie się 1RES, nie wymaga
żadnych komponentów (w miejscu gdzie powinny być komponenty nic nie ma i od razu
jest następny przecinek), a do stworzenia go potrzeba Alchemist's Supplies i
Cook's Utensils (dwa narzędzia rozdzielone przecinkiem są wzięte w cudzysłów).

# Jak uruchomić lokalnie w celu wprowadzania zmian
Po pierwsze, trzeba pobrać kod tej strony. Osoby które używają gita muszą zrobić
clone; mogą też zrobić najpierw fork.

Pozostali mogą pobrać zip z kodem używając [tego
linku](https://gitlab.com/draganczukp/dnd/-/archive/master/dnd-master.zip)

## Wymagane oprogramowanie
Do uruchomienia lokalnej wersji potrzebne jest kilka programów:

- [Node.js](https://nodejs.org/en/) - środowisko uruchomieniowe dla Yarna
- [Yarn](https://yarnpkg.com/getting-started/install). Jedno z ważniejszych
	narzędzi. Możecie o nim myśleć jak o kleju, który utrzymuje wszystko w kupie.
	Kiedyś na tej stronie był po prostu `.exe`, ale niestety już nie ma.
	Instalacja nie jest skomplikowana, ale jak ktoś nie jest programistą to może
	się poczuć trochę jak w matrixie. Kroki instalacji (node.js musi już być
	zainstalowane):
	- Otwórz menu start
	- Wpisz `cmd`
	- "Uruchom jako administrator"
	- W tym okienku wpisz `npm install -g yarn`
	- Gdy skończy się instalować możesz zamknąć to okno.
- Jakiś edytor tekstu, najlepiej taki który ogarnia programowanie. Osobiście
	używam Visual Studio Code, ale od biedy nawet zwykłym notatnikiem można.

## Pierwsze uruchomienie
Wypakuj kod do jakiegoś folderu. Polecam wypakować go do osobnego folderu na
`C:\`, np. `C:\dnd`. Ważne żeby pamiętać gdzie to jest.

Użytkownicy VS Code teraz mogą otworzyć ten folder (`Plik` -> `Otwórz folder`),
a następnie używając skrótu klawiszowego ``Ctrl+` `` (klawisz na lewo od 1)
otwierają wiersz poleceń.

Jeśli ktoś używa jakiegoś innego edytora dla programistów musi zrobić to samo,
ale to już we własnym zakresie.

Dla użytkowników zwykłego notatnika pozostaje używanie CMD (menu start ->
`cmd`). Trzeba w tym wejść do odpowiedniego folderu. Jeśli ktoś umie się tym posługiwać to idzie do następnego akapitu. Dla tych co nie umieją:
- Jeśli kod wypakowałeś gdzieś indziej niż na dysk `C:\` (np. na dysk `D:\`),
	musisz wpisać literę dysku i zaraz po niej dwukropek, czyli np. jeśli
	wypakowałeś na `D:\` to wpisujesz `d:`.
- Następnie za pomocą polecenia `cd` przechodzisz do folderu, w którym jest jest
	kod. Najprostszym sposobem na to powinno być wpisanie `cd`, kliknięcie spacji
	i przeciągnięcie tego folderu.
	- Jeżeli ten folder jest wypakowany do `C:\dnd`, możesz po prostu wpisać `cd
		c:\dnd`
- Za pomocą polecenia `dir` zweryfikuj, że jesteś w dobrym folderze. Wynikiem
	powinna być lista plików w folderze.
	- Jeżeli jej nie widzisz, możliwe że masz folder w folderze, czyli `dir`
		wyświetli ci tylko pojedynczy folder `dnd`. Wtedy użyj `cd` żeby do niego
		wejść: `cd dnd`.

#### Wszystkie dalsze instrukcje zakładają że polecenia są wykonywane w tym folderze

Teraz jak już jesteśmy w odpowiednim miejscu należy wpisać polecenie
```
yarn
```
Zainstaluje to wszystko co jest wymagane do uruchomienia lokalnego serwera.
Możesz już przejść do następnego kroku.

## Odpalenie lokalnego serwera
Mamy kod, jesteśmy w odpowiednim folderze i zainstalowaliśmy wszystko co
potrzebne. Teraz pozostaje tylko uruchomienie serwera poleceniem
```
yarn dev
```

Będziesz używać tego polecenia za każdym razem, gdy będziesz uruchamiać lokalną
wersję kodu.

Powinno troszkę pomyśleć, po czym otworzy się twoja domyślna przeglądarka od
razu z tą stroną.

Żeby przetestować, czy na pewno działa, wejdź do pliku `src/scss/_colors.scss` i
pozmieniaj kolory na jakieś losowe (możesz wpisać angielskie nazwy kolorów). Jak tylko zapiszesz plik przeglądarka powinna się odświeżyć i
zobaczysz swoje zmiany. Oczywiście wycofaj je zanim zaczniesz pracę.

