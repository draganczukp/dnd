FROM node:14 as build

WORKDIR /app

COPY . .

RUN yarn install
RUN yarn build

FROM nginx as app

COPY --from=build /app/dist /usr/share/nginx/html/dist
COPY --from=build /app/fonts /usr/share/nginx/html/fonts
COPY --from=build /app/index.html /app/items.html /app/rewards.html /app/better20.html /app/favicon.ico /app/magic_items_new.csv /app/resources.json /app/resources.html /usr/share/nginx/html/
