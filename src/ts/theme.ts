let theme: "dark"|"default";

const getTheme = (): "dark"|"default" => {
	return localStorage["theme"] = (localStorage["theme"] || "default");
}

const toggleTheme = (btn: HTMLButtonElement) => {
	if(theme === "dark"){
		localStorage["theme"] = theme = "default";
	} else {
		localStorage["theme"] = theme = "dark";
	}

	btn.innerText = theme == "dark" ? "☀️" : "🌙";
	updateHTML();
}

const updateHTML = () => {
	const html = document.querySelector("html")!;
	html.classList.remove("theme--dark");
	html.classList.remove("theme--default");

	html.classList.add(`theme--${theme}`);
}

	theme = getTheme();
	updateHTML();

setTimeout(()=>{
	const btn = document.createElement("button");
	btn.style.position = "fixed";
	btn.style.top = "5px";
	btn.style.right = "5px";

	btn.innerText = theme == "dark" ? "☀️" : "🌙";

	btn.addEventListener("click", ()=>toggleTheme(btn));

	document.body.append(btn);
}, 1);
