interface Data {
  name: string;
  type: string;
  req: string;
  att: boolean;
  res: number;
  gp: number;
  ct: number;
  cm: number;
  dis: number;
  comp: string;
  tool: string;
}

let data: Data[] = [];
let displayData: Data[];
let currSort: string = "name";

const rowToData = (row: string): Data => {
	//TODO: Trzeba listę wyczyścić z tych kolumn
  const [name, type, req, att, res, gp, ct, cm, dis, comp, tool] = row
  	.split("|")
	.map(s => s.trim());

	return {
    name,
    att: att === "✗" ? false : true,
	type,
    res: Number.parseInt(res),
    gp: Number.parseInt(res)/2,
    ct: calcCT(Number.parseInt(res)),
    cm: calcCM(Number.parseInt(res)),
    dis: Number.parseInt(res)/2,
    req,
    tool,
	comp,
  };
};

const calcCM = (res: number): number => {
	if(res < 500)
		return 3;
	if(res < 1_000)
		return 4;
	if(res < 2_000)
		return 5;
	if(res < 3_000)
		return 7;
	if(res < 4_000)
		return 9;
	if(res < 16_000)
		return 11;
	if(res < 25_000)
		return 13;
	if(res < 50_000)
		return 15;

	return 17;
}

const calcCT = (res: number): number => {
	if(res < 500)
		return 1;
	if(res < 1_000)
		return 2;
	if(res < 2_000)
		return 4;
	if(res < 3_000)
		return 6;
	if(res < 4_000)
		return 8;
	if(res < 16_000)
		return 10;
	if(res < 25_000)
		return 20;
	if(res < 50_000)
		return 30;

	return 35;
}


const loadData = async (): Promise<Data[]> => {
  return (await (await fetch("/magic_items_new.csv")).text()).split(/\n/)
  	.filter(s => s.length !== 0)
	.filter(s => s !== '||||||||||')
	// .slice(1)
	.map(rowToData);
};

const TD = (s: string|boolean|number): HTMLTableDataCellElement => {
	const td = document.createElement("td");
	switch(typeof s){
		case "string":
			td.innerText = s;
			break;
		case "boolean":
			td.innerText = s ? "Y" : "N";
			break;
		case "number":
			td.innerText = s.toLocaleString();
			break;
	}
	return td;
};

const TR = (data: Data): HTMLTableRowElement => {
	const tr = document.createElement("tr");

	tr.appendChild(TD(data.type));
	tr.appendChild(TD(data.name));
	tr.appendChild(TD(data.req));
	tr.appendChild(TD(data.att));
	tr.appendChild(TD(data.res));
	tr.appendChild(TD(data.gp));
	tr.appendChild(TD(data.ct));
	tr.appendChild(TD(data.cm));
	tr.appendChild(TD(data.dis));
	tr.appendChild(TD(data.comp));
	tr.appendChild(TD(data.tool));

	return tr;
}

const renderData = (): void => {
	const tbody = document.querySelector("#items")!;

	while (tbody.childElementCount > 1) {
		tbody.removeChild(tbody.lastChild!);
	}


	displayData.map(TR)
		.forEach(tr => tbody.appendChild(tr));
};

const sorter = (property: string): ((a: Data, b: Data)=>number) => {
    let sortOrder = 1;

    if(property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }

    return (a: Data,b: Data) =>((a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0 ) * sortOrder;
}

const onColumnClick = (ev: MouseEvent) => {
	const elt = ev.target as HTMLElement;
	currSort = elt.dataset.col || "name";


	displayData = data.sort(sorter(currSort));

	renderData();
}

const filter = (d: Data, val: string): boolean => {
	val = val.replace('\+', '\\+');
	const regex = new RegExp(`.*${val}.*`, "i");
	return !!d.name.match(regex);
}

const onSearch = (ev: Event) => {
	const elt = ev.target as HTMLInputElement;
	const val = elt.value;

	displayData = data
				.filter(d => filter(d, val))
				.sort(sorter(currSort));

	renderData();
}

(async () => {
  data = await loadData();
  displayData = data.sort(sorter(currSort));

  renderData();

  document.querySelectorAll("th")
	  .forEach(elt => elt.addEventListener("click", onColumnClick));

  const input = document.querySelector<HTMLInputElement>("input#search");
  input?.addEventListener("input", onSearch);
})();
