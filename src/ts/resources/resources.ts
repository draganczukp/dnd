import Resource from "./Resource.js";

const getResources = async (): Promise<Resource[]> => {
	try{
		const resp = await fetch("/resources.json");
		return await resp.json();
	} catch (e) {
		console.error(e);
		return [];
	}
};

const toElement = (res: Resource): HTMLDivElement => {
	const div = document.createElement("div");
	const h2 = document.createElement("h2");
	const a = document.createElement("a");
	const span = document.createElement("span");
	const p = document.createElement("p");

	div.classList.add("resource-link");

	a.href = res.url;
	a.innerText = res.name;
	h2.appendChild(a);
	div.appendChild(h2);

	span.innerText = `Kategoria: ${res.category}`;
	div.appendChild(span);

	p.innerText = res.description;
	div.appendChild(p);

	return div;
};

(async ()=>{
	const data = await getResources();
	const elt = document.querySelector<HTMLDivElement>("#elements") || document.createElement("div");

	data.map(toElement)
		.forEach(e => elt.appendChild(e));
})();