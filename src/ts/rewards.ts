const calculatePlayer = () => {
	const xpElt = document.querySelector<HTMLInputElement>("#xp");
	const xp = xpElt?.valueAsNumber || 0;

	const minGold = xp * 0.25;
	const maxGold = xp * 0.5;

	showPlayer(minGold, maxGold);
}

const calculateDM = ()=>{
	const dmTierElt = document.querySelector<HTMLInputElement>("#dmtier");
	const timeElt = document.querySelector<HTMLInputElement>("#time");
	const halfElt = document.querySelector<HTMLInputElement>("#half");

	const dmTier = dmTierElt?.value || "1";
	const half = halfElt?.checked || false;
	const time: number = (timeElt?.valueAsNumber || 0) + (half ? 0.5 : 0);

	let dmXPMod = 1;

	switch(dmTier) {
		case "1":
			dmXPMod = 500;
			break;
		case "2":
			dmXPMod = 1000;
			break;
		case "3":
			dmXPMod = 2000;
			break;
		case "4":
			dmXPMod = 3000;
			break;
		case "5":
			dmXPMod = 5000;
			break;
		case "6":
			dmXPMod = 10000;
			break;
	}

	const dmXP = time * dmXPMod;
	const dmGP = dmXP * 0.5;
	const dmDT = time * 3;

	showDM(dmXP, dmGP, dmDT);
}

const showPlayer = (minGP: number, maxGP: number) => {
	const resultElt = document.querySelector("#result")!;

	resultElt.innerHTML = `<span class="bold">Min GP </span> <span>${minGP.toFixed(2)}</span>
	<br><span class="bold">Max GP: </span><span>${maxGP.toFixed(2)}</span>`;
}

const showDM = (dmXP: number, dmGP: number, dmDT: number) => {
	const resultElt = document.querySelector("#resultDM")!;

	resultElt.innerHTML = `<span class="bold">Nagrody dla postaci DMa:<br/></span>
	<span>${dmXP.toFixed(0)}XP, ${dmGP.toFixed(0)}GP i ${dmDT}DT`;
}

(async ()=>{
	document.querySelector("#submit")?.addEventListener("click", calculatePlayer)
	document.querySelector("#submitDM")?.addEventListener("click", calculateDM)
})();
